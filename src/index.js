import React from 'react';
import ReactDom from 'react-dom';
import PlayGame from './components/Game';

document.addEventListener('DOMContentLoaded',() => {
  ReactDom.render(
      <PlayGame />,
    document.getElementById('app')
  );
});
