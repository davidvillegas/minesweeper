import React, { Component } from 'react';
import Square from '../Square'

class Board extends Component{
  constructor(props){
    super(props);

  }

  renderSquare(x, y) {
    return <Square valuex={x} valuey={y}/>;
  }

  createBoard(total){
    let board = [];
    for (let i = 0; i < total; i++) {
        board.push(i);
      }
    return board;
  }

  render() {
    let boardRow =  this.createBoard(this.props.rows);
    let boardCol = this.createBoard(this.props.cols);
    return (

        <div className="board">
          {boardRow.map((x,i)=>
            <div className="row">
              {boardCol.map((y,i)=>
                this.renderSquare(x, y)
              )}
            </div>
          )}
        </div>

    )
  }
}

export default Board;
