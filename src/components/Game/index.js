import React, { Component } from 'react';
import Board from '../Board';
import HeadBoard from '../HeadBoard';

class PlayGame extends Component{
  constructor(props){
    super(props);
    this.state = {
      rows: 10,
      cols: 10,
      mines: 10,
      flags: 10,
      time: 0
    }
  }

  render () {
    return (
      <div className="container mt-5">
        <div className="row">
          <h1>MineSweeper</h1>
        </div>
        <div className="row">
          <HeadBoard time={this.state.time} flagCount={this.state.flags} />
        </div>
        <div className="row">
          <Board rows={this.state.rows} cols={this.state.cols} mines={this.state.mines}/>
        </div>
      </div>
    )
  }

}

export default PlayGame;
