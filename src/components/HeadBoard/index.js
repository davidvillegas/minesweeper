import React, { Component } from 'react';

class HeadBoard extends Component{
  constructor(){
    super();
  }

  render() {
    let minutes = Math.floor(this.props.time / 60);
    let seconds = this.props.time - minutes * 60 || 0;
    let formattedSeconds = seconds < 10 ? `${seconds}` : seconds;
    let time = `${minutes}:${formattedSeconds}`;
    return(
      <div className="board-head">
        <div className="flag-count">{this.props.flagCount}</div>
        <button className="btn btn-default">Reset</button>
        <div className="timer">{time}</div>
      </div>
    )
  }

}

export default HeadBoard;
